exports.dateFormatter = (date) => {
    let datefo = new Date(date)
    let day = datefo.getDate()
    let month = datefo.getMonth() + 1
    let year = datefo.getFullYear()
    let datef = ''
    if (month < 10) {
        datef = `${day}-0${month}-${year}`
    } else {
        datef = `${day}-${month}-${year}`
    }
    return datef
}
