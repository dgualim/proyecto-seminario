const fs = require('fs')
const path = require('path')
const puppeteer = require('puppeteer')
const handlebars = require('handlebars')

exports.createPDF = async (data, templatepath) => {
    console.log('DATA' + JSON.stringify(data))
    console.log('templatepath' + templatepath)

    var templateHtml = fs.readFileSync(path.join(process.cwd(), templatepath), 'utf8')
    var template = handlebars.compile(templateHtml)
    var html = template(data)

    var milis = new Date()
    milis = milis.getTime()

    var pdfPath = path.join('src/pdf/', `${data.nombre}${data.apellido}-${milis}.pdf`)

    var options = {
        format: 'Letter',
        headerTemplate: '<p></p>',
        footerTemplate: '<p></p>',
        displayHeaderFooter: false,
        margin: {},
        printBackground: true
    }

    const browser = await puppeteer.launch({
        args: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-gpu'],
        headless: true
    })

    var page = await browser.newPage()

    await page.goto(`data:text/html;charset=UTF-8,${html}`, {
        waitUntil: ['domcontentloaded', 'networkidle0', 'load']
    })

    const pdf = await page.pdf(options)
    await browser.close()
    return pdf
}
