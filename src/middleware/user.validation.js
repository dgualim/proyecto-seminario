const Joi = require('joi')

function validationUser() {
    return async (req, res, next) => {
        try {
            const schema = Joi.object({
                email: Joi.string().required(),
                username: Joi.string().required(),
                password: Joi.string().required(),
                type: Joi.string().required()
            })
            try {
                await schema.validateAsync(req.body)
                next()
            } catch (e) {
                res.status(406)
                res.setHeader('Content-Type', 'application/problem+json')
                res.send({
                    type: 'about:blank',
                    title: 'Not Acceptable',
                    status: 406,
                    detail: e.details
                })
            }
        } catch (e) {
            res.status(500)
            res.setHeader('Content-Type', 'application/problem+json')
            res.send({
                type: 'about:blank',
                title: 'Internal Server Error',
                status: 500,
                detail: 'An internal server error ocurred'
            })
        }
    }
}
module.exports = validationUser
