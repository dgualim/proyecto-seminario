const RecetaMedica = require('../models/recetaMedica.model')
const pdfGenerator = require('../pdf/pdf-generator.js')
const dateFormatter = require('../utils/date-formatter.utils')

exports.getAllRecetasMedicas = (req, res) => {
    RecetaMedica.getAllRecetasMedica((err, recetas) => {
        if (err) res.send(err)
        console.log('Recetas', recetas)
        res.send(recetas)
    })
}

exports.createRecetaMedica = (req, res) => {
    console.log(req.body)
    const recetaMedi = new RecetaMedica(req.body)
    console.log(recetaMedi)

    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        return res.status(400).send({ success: false, message: 'Ingrese los datos necesarios' })
    } else {
        RecetaMedica.createRecetaMedica(recetaMedi, (err, recetas) => {
            if (err) res.send(err)
            res.json({ success: true, message: 'Recetas creada exitosamente', data: recetas })
        })
    }
}

exports.deleteRecetaMedica = (req, res) => {
    RecetaMedica.deleteRecetaMedica(req.params.id, (err, recetaMedica) => {
        if (err) res.send(err)
        console.log('Receta', recetaMedica)
        res.send(recetaMedica)
    })
}

exports.createRecetaMedicaPdf = async (req, res) => {
    RecetaMedica.getRecetaMedicaPdf(req.body.id_receta_medica, async (err, pdfCita) => {
        const newConsultationDataFormatted = new RecetaMedica(pdfCita[0])
        const pdf = await pdfGenerator.createPDF(
            newConsultationDataFormatted,
            'src/pdf/template-nuevaconsulta.html'
        )
        console.log('termine')
        res.contentType('application/pdf')
        res.send(pdf)
    })
}
