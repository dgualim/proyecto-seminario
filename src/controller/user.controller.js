const UserModel = require('../models/user.model');

const bcrypt = require('bcrypt');

//get all users list
exports.getAllUsers = (req, res) => {
    UserModel.getAllUsers((err, users) => {
        if (err) res.send(err);
        console.log('Usuarios', users);
        res.send(users);
    });
}

//Login
exports.Login = (req, res) => {
    const userData = req.body;
    console.log(req.body.password);
    
    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        return res.status(400).send({ success: false, message: 'Ingrese los parametros necesarios' });
    } else {
        UserModel.Login(userData, (err, users) => {
            console.log(users);
            if(users[0] != null){
                let passwordIsValid = bcrypt.compareSync(req.body.password, users[0].password);
                if (!passwordIsValid)
                {
                    return res.status(401).send({ success: false, message: 'Password Incorrecto' });
                } 
                    if (err)
                        res.send(err);
                    res.json({success:true, message: 'Login exitoso', data: users});
            }
            else 
            return res.status(401).send({ success: false, message: 'Login incorrecto' });
        });
    }
}

//get user by id
exports.getUserById = (req, res) => {
    UserModel.getUserById(req.params.id, (err, user) => {
        if (err)
            res.send(err);
        console.log('Usuario', user);
        user.type=UserTypeModel.
        res.send(user);
    });
}

//get user by username
exports.getUserByUsername = (req, res) => {
    UserModel.getUserByUsername(req.params.username, (err, user) => {
        if (err) res.send(err);
        console.log('Usuario', user);
        res.send(user);
    });
}

//get user by email
exports.getUserByEmail = (req, res) => {
    UserModel.getUserByEmail(req.params.email, (err, user) => {
        if (err) res.send(err);
        console.log('Usuario', user);
        res.send(user);
    });
}

//create a new user
exports.createNewUser = (req, res) => {
    console.log(req.body);
    const userData = new UserModel(req.body);
    console.log(userData);
    
    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        return res.status(400).send({ success: false, message: 'Ingrese los datos necesarios' })
    } else {
        UserModel.createNewUser(userData, (err, users) => {
            if (err)
                res.send(err);
            res.json({success:true, message: 'Usuario creado exitosamente', data: users});
        });
    }
}

//get user by email
exports.deleteUser = (req, res) => {
    UserModel.deleteUser(req.params.id, (err, user) => {
        if (err)
            res.send(err);
        console.log('Usuario', user);
        res.send(user);
    });
}