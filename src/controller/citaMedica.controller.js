const CitaMedica = require('../models/citaMedica.model')
const dateFormatter = require('../utils/date-formatter.utils')

exports.getAllCitasMedica = (req, res) => {
    CitaMedica.getAllCitasMedicas((err, citasMedicas) => {
        if (err) res.send(err)
        console.log('citasMedicas -> ', citasMedicas)
        res.send(citasMedicas)
    })
}

exports.createCitasMedica = (req, res) => {
    console.log(req.body)
    const dataCitaMed = new CitaMedica(req.body)

    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        return res.status(400).send({ success: false, message: 'Ingrese los datos necesarios' })
    } else {
        CitaMedica.createCitasMedicas(dataCitaMed, (err, citaMedica) => {
            if (err) res.send(err)
            res.json({ success: true, message: 'CitaMedica creada exitosamente', data: citaMedica })
        })
    }
}

exports.deleteCitaMedica = (req, res) => {
    CitaMedica.deleteCitaMedica(req.params.id, (err, citaMedica) => {
        if (err) res.send(err)
        console.log('CitaMedica', citaMedica)
        res.send(citaMedica)
    })
}
