var dbConn = require('../db/db-connection')

var Receta = function (receta) {
    this.id_receta_medica = receta.id_receta_medica
    this.nombre = receta.nombre
    this.edad = receta.edad
    this.diagnostico = receta.diagnostico
    this.fecha = dateFormat()
    this.medicamentos = receta.medicamentos
    this.apellido = receta.apellido
}

function dateFormat() {
    const date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
    return date
}

Receta.getAllRecetasMedica = (result) => {
    dbConn.query('SELECT * FROM tb_receta_medica', (err, res) => {
        if (err) {
            console.log('Error en Consulta Recetas', err)
            result(null, err)
        } else {
            console.log('Consulta Recetas Exitosa')
            result(null, res)
        }
    })
}

Receta.createRecetaMedica = (recetaData, result) => {
    dbConn.query('INSERT INTO tb_receta_medica SET ?', recetaData, (err, res) => {
        if (err) {
            console.log('Error al crear una Receta', err)
            result(null, err)
        } else {
            console.log('Receta creada exitosamente')
            result(null, res)
        }
    })
}

//delete user
Receta.deleteRecetaMedica = (recetaId, result) => {
    dbConn.query('DELETE FROM tb_receta_medica WHERE id_receta_medica=?', recetaId, (err, res) => {
        if (err) {
            console.log('Error al eliminar Receta', err)
            result(null, err)
        } else {
            console.log('Receta eliminada exitosameente')
            result(null, res)
        }
    })
}

//get last patient
Receta.getLastPatient = (result) => {
    dbConn.query('SELECT * FROM patient p ORDER BY Id DESC LIMIT 1;', (err, res) => {
        if (err) {
            console.log('Error while fetching patients', err)
            result(null, err)
        } else {
            console.log('Last Patient fetched succesfully')
            result(null, res)
        }
    })
}

Receta.dataPdf = (recetaId, result) => {
    dbConn.query(
        'SELECT *  FROM tb_receta_medica WHERE id_receta_medica=?',
        recetaId,
        (err, res) => {
            if (err) {
                console.log('Error al eliminar Receta', err)
                result(null, err)
            } else {
                console.log('Receta eliminada exitosameente')
                result(null, res)
            }
        }
    )
}

Receta.getRecetaMedicaPdf = (recetaId, result) => {
    dbConn.query(
        'SELECT * FROM  tb_receta_medica where id_receta_medica=?',
        recetaId,
        (err, res) => {
            if (err) {
                //console.log('Error en Consulta Receta', err)
                result(null, err)
            } else {
                console.log(res)
                console.log('Consulta Receta Exitosa')
                result(null, res)
            }
        }
    )
}

module.exports = Receta
