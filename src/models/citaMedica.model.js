var dbConn = require('../db/db-connection')

var CitaMedica = function (dataCita) {
    this.id_cita_medica = dataCita.id_cita_medica
    this.nombre_paciente = dataCita.nombre_paciente
    this.apellido_paciente = dataCita.apellido_paciente
    this.fecha_hora_cita = dataCita.fecha_hora_cita
    this.doctor_cita = dataCita.doctor_cita
    this.fecha_cita = dateFormat()
    this.estado = dataCita.estado
}

function dateFormat() {
    const date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
    return date
}

CitaMedica.getAllCitasMedicas = (resutl) => {
    dbConn.query('SELECT * FROM tb_cita_medica', (err, res) => {
        if (err) {
            console.log('Error en Consulta CitaMedica', err)
            result(null, err)
        } else {
            console.log('Consulta CitaMedica Exitosa')
            resutl(null, res)
        }
    })
}

CitaMedica.createCitasMedicas = (dataCitaMedica, result) => {
    dbConn.query('INSERT INTO tb_cita_medica SET ?', dataCitaMedica, (err, res) => {
        if (err) {
            console.log('Error al crear una CitaMedica', err)
            result(null, err)
        } else {
            console.log('CitaMedica creada exitosamente')
            result(null, res)
        }
    })
}

CitaMedica.deleteCitaMedica = (cidaMedidaId, result) => {
    dbConn.query('DELETE FROM tb_cita_medica WHERE id_cita_medica=?', cidaMedidaId, (err, res) => {
        if (err) {
            console.log('Error al eliminar CitaMedica', err)
            result(null, err)
        } else {
            console.log('CitaMedica eliminada exitosameente')
            result(null, res)
        }
    })
}

module.exports = CitaMedica
