const express = require('express')
const router = express.Router()

const recetaMedicaController = require('../controller/recetaMedica.controller')

//get all recetasMedicas
router.get('/', recetaMedicaController.getAllRecetasMedicas)

//create recetasMedicas
router.post('/', recetaMedicaController.createRecetaMedica)

//delete recetasMedicas
router.delete('/:id', recetaMedicaController.deleteRecetaMedica)

//pdf recetasMedicas
router.post('/pdf', recetaMedicaController.createRecetaMedicaPdf)

module.exports = router
