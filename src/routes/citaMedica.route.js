const express = require('express')
const router = express.Router()
const citaMedicaController = require('../controller/citaMedica.controller')

//get all citasMedicas
router.get('/', citaMedicaController.getAllCitasMedica)

//crete citasMedicas
router.post('/', citaMedicaController.createCitasMedica)

//delete citasMedicas
router.delete('/:id', citaMedicaController.deleteCitaMedica)

module.exports = router
