
FROM node:14.16

RUN apt-get update && apt-get install -yq libgconf-2-4
#2. INSTALL LIBRERIA DE DEPENDENCIAS
RUN apt-get update && apt-get install -y wget --no-install-recommends \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install -y google-chrome-unstable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst ttf-freefont \
      alien libaio1 -y apt-utils unzip telnet  \
    git build-essential python  \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get purge --auto-remove -y curl \
    && rm -rf /src/*.deb

ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64 /usr/local/bin/dumb-init
RUN chmod +x /usr/local/bin/dumb-init    
#3. CLEAN ARCHIVOS TEMPORALES
RUN apt-get clean  \ 
    && rm -rf /var/lib/apt/lists/* 
#4. CREA DIRECTORIOS WORK DIR
RUN mkdir -p /app/fundacionapa-api
RUN mkdir -p /app/fundacionapa-api/log
WORKDIR /app/fundacionapa-api
#5. COPYA PACKATE JSON E INSTALLA DEPENDENCIAS NODE.JS
COPY package.json /app/fundacionapa-api/
RUN npm install
#6. COPYA ARCHIVOS A CARPETAS WORKDIR
COPY . /app/fundacionapa-api
ENV NODE_ENV ${ENVIRONMENT}
#7. EXPONE SERVICIO PUERTO 3000
EXPOSE 3000
ENTRYPOINT ["dumb-init", "--"]
#ENV NODE_ENV=production
CMD [ "npm", "start" ]
